# Bank Project

**This bank project is a Java Desktop Application.** It's the place that brings together all of bank's logic around data.

## ⚡️ Requirements

- A solid interface with [JavaFX](https://openjfx.io/) (`v11`)
- A superset of [Java](https://www.java.com/en/) (`v11` or newer)
- A strong data persistence with [Hibernate](https://hibernate.org/)
- A simple database with [MySQL](https://www.docker.com/)

## 🔥 Stack

- [Maven](https://maven.apache.org/), a software project management and comprehension tool.

## 💻 How to use

After cloning the repository, you need to configure the database:

At the root of the project you will find a `initialize_database.sql ` file, it contains all the necessary commands to configure your database with a user `simplon` and password `simplon`.

After the configuration of the application, you need to run it. Therefore, use the following command:

```shell
$ mvn javafx:run
```

Now you need try our application!

## 🕺 Contribute

**Want to hack on `Bank Project`? Follow the next instructions:**

1. Fork this repository to your own git account and then clone it to your local device
2. Install dependencies as we explained at this documentation.
4. Send a pull request 🙌

Good luck, developer! 🚀
