package projectbank.domain;

public class Customer extends User {
    private Banker banker;
    private int id;
    private int zip;
    private String city;
    private String country;
    private String address;

    public Customer() {
        super();
    }

    public Customer(String lastName, String firstName, String country, String mail, String password, String address, int zip, String phone, String city) {
        super(firstName, lastName, phone, mail, password);

        this.zip = zip;
        this.city = city;
        this.address = address;
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Banker getBanker() {
        return this.banker;
    }

    public int getZip() {
        return this.zip;
    }

    public String getCity() {
        return this.city;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
