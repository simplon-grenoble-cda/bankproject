package projectbank.domain;

public class Account {
    private Customer customer;
    private String accountNumber;
    private String iban;
    private float balance;
    private String createdAt;
    private String status;
    private int customerId;

    public Account() {
    }

    public Account(Customer customer, String accountNumber, String iban, float balance, String createdAt, String status) {
        this.customer = customer;
        this.accountNumber = accountNumber;
        this.iban = iban;
        this.balance = balance;
        this.createdAt = createdAt;
        this.status = status;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public String getAccountNumber() {
        return this.accountNumber;
    }

    public String getIban() {
        return iban;
    }

    public float getBalance() {
        return balance;
    }

    public String getStatus() {
        return status;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }
}
