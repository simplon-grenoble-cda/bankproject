package projectbank.data;

import projectbank.domain.Customer;

import java.sql.*;

public class CustomerDAO {
    private static Connection connection;

    /**
     * Executes the sql query to add customer to the database
     *
     * @param customer
     */
    public static void addCustomer(Customer customer) {

        String tableName = "Customer";
        String lastName = customer.getLastName();
        String firstName = customer.getFirstName();
        String email = customer.getMail();
        String passWord = customer.getPassword();
        String adress = customer.getAddress();
        String city = customer.getCity();
        String phone = customer.getPhone();
        int zip = customer.getZip();

        String addcustomer = "INSERT INTO " + tableName + " (first_name, last_name,address,zip,city,country,phone,mail,password) VALUES ('" + firstName + "','" + lastName + "','" + adress + "','" + zip + "','" + city + "','" + "france" + "','" + phone + "','" + email + "','" + passWord + "')";

        try {
            Statement statement = DbConnection.getStatement();
            int i = statement.executeUpdate(addcustomer);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * @param mail
     * @param password
     * @return Customer: it returns the logged in customer
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static Customer checkLogin(String mail, String password) throws SQLException, ClassNotFoundException {

        Connection connection = DbConnection.getConnection();

        String sql = "SELECT * FROM Customer WHERE mail = ? and password = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, mail);
        statement.setString(2, password);

        ResultSet result = statement.executeQuery();

        Customer customer = null;

        if (result.next()) {
            customer = new Customer();
            customer.setLastName(result.getString("last_name"));
            customer.setMail(mail);
        }
        return customer;
    }

    /**
     * @param id
     * @return customer by Id after executing the SQL query
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static Customer getCustomerById(int id) throws SQLException, ClassNotFoundException {

        String sql = "SELECT * FROM Customer WHERE id = ? ";
        PreparedStatement statement = DbConnection.getConnection().prepareStatement(sql);
        statement.setInt(1, id);

        ResultSet result = statement.executeQuery();

        Customer customer = null;

        if (result.next()) {
            customer = new Customer();
            customer.setLastName(result.getString("last_name"));
            customer.setFirstName(result.getString("first_name"));
            customer.setAddress(result.getString("address"));
            customer.setZip(result.getInt("zip"));
            customer.setCity(result.getString("city"));
            customer.setCountry(result.getString("country"));
            customer.setPhone(result.getString("phone"));
            customer.setMail(result.getString("mail"));
        }
        return customer;
    }

    /**
     * @param mail
     * @param password
     * @return customer
     * @throws SQLException this method to get the customer from the database
     */
    public static Customer getCustomer(String mail, String password) throws SQLException {
        String sql = "SELECT * FROM Customer WHERE mail = ? and password = ?";
        PreparedStatement statement = DbConnection.getConnection().prepareStatement(sql);
        statement.setString(1, mail);
        statement.setString(2, password);
        ResultSet result = statement.executeQuery();

        Customer customer = null;

        if (result.next()) {
            customer = new Customer();
            System.out.println(result.getInt("id"));
            customer.setId(result.getInt("id"));
        }
        return customer;
    }
}

