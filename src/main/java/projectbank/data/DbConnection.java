package projectbank.data;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DbConnection {

    private static Connection connection = null;
    private static Statement statement = null;

    /**
     * @return connection to the database
     */
    public static Connection getConnection() {
        if (connection == null) {
            connectDB();
        }
        return connection;
    }

    public static Statement getStatement() {
        try {
            statement = getConnection().createStatement();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return statement;
    }

    /**
     * the connection to he database using the configuration in application.properties file
     */
    public static void connectDB() {

        String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String appConfigPath = rootPath + "application.properties";
        Properties appProps = new Properties();
        try {
            appProps.load(new FileInputStream(appConfigPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String dbName = appProps.getProperty("database");
        String user = appProps.getProperty("user");
        String password = appProps.getProperty("password");
        String host = appProps.getProperty("host");
        try {
            String urlConnection = "jdbc:mysql://" + host + "/" + dbName + "?user="
                    + user + "&password=" + password;
            connection = DriverManager.getConnection(urlConnection);

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

}
