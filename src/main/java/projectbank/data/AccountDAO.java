package projectbank.data;

import projectbank.domain.Account;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AccountDAO {

    /**
     * @return list of all accounts waiting for validation by Banker
     * @throws SQLException
     */
    public static List<Account> getAccountsToValidate() throws SQLException {

        String sql = "SELECT * FROM Account WHERE status_c = 'EN_ATTENTE'";
        PreparedStatement statement = DbConnection.getConnection().prepareStatement(sql);

        ResultSet result = statement.executeQuery();

        List<Account> accounts = new ArrayList<>();

        while (result.next()) {
            Account account = new Account();
            account.setCustomerId(result.getInt("customer_id"));
            account.setAccountNumber(result.getString("account_number"));
            account.setIban(result.getString("iban"));
            account.setCreatedAt(result.getString("created_at"));

            accounts.add(account);
        }
        return accounts;
    }

    /**
     * changes the account status after banker decision
     * @param status    the account can be VALIDE or REFUSE
     * @param numCompte
     * @throws SQLException
     */
    public static void validateAccount(String status, String numCompte) throws SQLException {

        String sql = "UPDATE Account SET status_c = '" + status + "' " +
                "WHERE account_number = '" + numCompte + "'";

        Statement statement = DbConnection.getStatement();
        statement.executeUpdate(sql);
    }

    /**
     * Executes the sql query to add account to the database
     * @param account1
     * @throws SQLException
     */
    public static void addAccount(Account account1) throws SQLException {

        String sql = "INSERT INTO Account (`customer_id`, `account_number`, `iban`, `balance`, `created_at`, `status_c`) VALUES  (?, ?, ?, ?, ?, ?)";
        PreparedStatement statement = DbConnection.getConnection().prepareStatement(sql);

        statement.setInt(1, account1.getCustomer().getId());
        statement.setString(2, account1.getAccountNumber());
        statement.setString(3, account1.getIban());
        statement.setFloat(4, account1.getBalance());
        statement.setString(5, account1.getCreatedAt());
        statement.setString(6, account1.getStatus());

        statement.executeUpdate();
    }
}
