package projectbank.presentation.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import projectbank.business.LoginCustomerService;
import projectbank.domain.Customer;
import projectbank.presentation.view.ScreenHelper;

import java.io.IOException;
import java.sql.SQLException;

public class LoginCustomerController {

    @FXML
    private TextField inputMail;
    @FXML
    private TextField inputPassword;
    @FXML
    private Label errorMessage;


    public void onClickConfirm() throws IOException {

        Customer customer = null;
        try {
            customer = LoginCustomerService.loginCustomer(inputMail.getText(), inputPassword.getText());

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        if (customer == null) {
            errorMessage.setText("adresse mail ou mot de pass incorrect, veuillez re-essayer.");
        } else {
            ScreenHelper.getSingleton().switchScreen("homepageBeforeValidation");
        }
    }

    public void onClickCancel() {

        ScreenHelper.getSingleton().switchScreen("home");
    }
}