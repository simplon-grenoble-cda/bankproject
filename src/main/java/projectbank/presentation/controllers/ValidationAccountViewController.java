package projectbank.presentation.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import projectbank.business.AccountValidationService;
import projectbank.data.AccountDAO;
import projectbank.data.CustomerDAO;
import projectbank.domain.Account;
import projectbank.domain.Customer;
import projectbank.presentation.view.ScreenHelper;

import javax.swing.*;
import java.sql.SQLException;
import java.util.List;

public class ValidationAccountViewController {

    @FXML
    private ListView accountListView;
    @FXML
    private TextField tfDateCreation;
    @FXML
    private TextField tfAccountNumber;
    @FXML
    private TextField tfMail;
    @FXML
    private TextField tfPhone;
    @FXML
    private TextField tfCity;
    @FXML
    private TextField tfCountry;

    ObservableList<Account> accountValidationToDisplay = FXCollections.observableArrayList();

    @FXML
    private void initialize() {

        accountListView.setItems(this.accountValidationToDisplay);

/**
 * Setup the text to be displayed in each list cell (the ListView object
 * cannot guess what to display from the Account object)
 */

        accountListView.setCellFactory(listView -> new ListCell<Account>() {
            @Override
            public void updateItem(Account account, boolean empty) {
                super.updateItem(account, empty);
                if (empty) {
                    setText(null);
                } else {
                    Customer customerById = null;
                    try {
                        customerById = CustomerDAO.getCustomerById(account.getCustomerId());
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    setText(customerById.getFirstName() + " " + customerById.getLastName());
                }
            }
        });

    }

    public void loadAllAccountValidationOnAction() {
        List<Account> accountList = AccountValidationService.listAllAccountValidation();
        this.accountValidationToDisplay.setAll(accountList);
    }

    public void onItemClick() {
        Account accountClicked = (Account) accountListView.getSelectionModel().getSelectedItem();

        if (accountClicked != null) {
            Customer customerById = null;
            try {
                customerById = CustomerDAO.getCustomerById(accountClicked.getCustomerId());
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            tfDateCreation.setText(accountClicked.getCreatedAt().toString());
            tfAccountNumber.setText(accountClicked.getAccountNumber());
            tfMail.setText(customerById.getMail());
            tfPhone.setText(customerById.getPhone());
            tfCity.setText(customerById.getCity());
            tfCountry.setText(customerById.getCountry());
        }
    }

    public void onClickValider() {

        int dialogResult = JOptionPane.showConfirmDialog(null, "Voulez vous valider ce compte?");
        if (dialogResult == JOptionPane.YES_OPTION) {
            try {
                AccountDAO.validateAccount("VALIDE", tfAccountNumber.getText());
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            refreshList();
        }
    }

    public void onClickRefuser() {

        int dialogResult = JOptionPane.showConfirmDialog(null, "Voulez vous refuser ce compte?");
        if (dialogResult == JOptionPane.YES_OPTION) {
            try {
                AccountDAO.validateAccount("REFUSE", tfAccountNumber.getText());
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            refreshList();
        }
    }

    public void refreshList() {
        loadAllAccountValidationOnAction();

        tfDateCreation.setText("");
        tfAccountNumber.setText("");
        tfMail.setText("");
        tfPhone.setText("");
        tfCity.setText("");
        tfCountry.setText("");
    }
    public void onClickDeconnect() {
        ScreenHelper.getSingleton().switchScreen("home");
    }
}

