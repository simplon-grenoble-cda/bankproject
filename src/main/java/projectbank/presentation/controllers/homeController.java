package projectbank.presentation.controllers;

import javafx.event.ActionEvent;
import projectbank.presentation.view.ScreenHelper;

public class homeController {

    public void onClickRegister(ActionEvent actionEvent) {
        ScreenHelper.getSingleton().switchScreen("register");
    }

    public void onClickLogin(ActionEvent actionEvent) {
        ScreenHelper.getSingleton().switchScreen("loginCustomer");
    }

    public void onClickLoginBanker(ActionEvent actionEvent) {
        ScreenHelper.getSingleton().switchScreen("loginBanker");
    }
}
