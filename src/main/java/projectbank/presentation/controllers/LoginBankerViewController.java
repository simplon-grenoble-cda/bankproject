package projectbank.presentation.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import projectbank.business.LoginBankerService;
import projectbank.business.LoginCustomerService;
import projectbank.domain.Banker;
import projectbank.domain.Customer;
import projectbank.presentation.view.ScreenHelper;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class LoginBankerViewController {

    @FXML
    private TextField mail;

    @FXML
    private TextField password;

    @FXML
    private Label failLabel;

    @FXML
    private void initialize() {
    }

    public void onClickConfirm() {

        Banker banker = null;
        try {
            banker = LoginBankerService.loginBanker(mail.getText(), password.getText());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        if (banker == null) {
            failLabel.setText("L'authentification à échoué");
        } else {
            ScreenHelper.getSingleton().switchScreen("validationAccount");

        }
    }

    public void onClickCancel() {

        ScreenHelper.getSingleton().switchScreen("home");
    }
}
