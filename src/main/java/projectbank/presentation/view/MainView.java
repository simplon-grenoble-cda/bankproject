package projectbank.presentation.view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class MainView extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        //configurate fxml pages
        Parent homeScreen = FXMLLoader.load(getClass().getResource("/fxml/home.fxml"));
        Parent registerScreen = FXMLLoader.load(getClass().getResource("/fxml/register.fxml"));
        Parent loginCustomerScreen = FXMLLoader.load(getClass().getResource("/fxml/loginCustomer.fxml"));
        Parent loginBankerScreen = FXMLLoader.load(getClass().getResource("/fxml/loginBanker.fxml"));
        Parent homepageBeforeValidationScreen = FXMLLoader.load(getClass().getResource("/fxml/homepageBeforeValidation.fxml"));
        Parent bankerDashboard = FXMLLoader.load(getClass().getResource("/fxml/bankerDashboard.fxml"));
        Parent validationAccount = FXMLLoader.load(getClass().getResource("/fxml/validationAccount.fxml"));

        ScreenHelper screenHelper = ScreenHelper.getSingleton();
        screenHelper.addScreen("home", homeScreen);
        screenHelper.addScreen("register", registerScreen);
        screenHelper.addScreen("loginCustomer", loginCustomerScreen);
        screenHelper.addScreen("loginBanker", loginBankerScreen);
        screenHelper.addScreen("homepageBeforeValidation", homepageBeforeValidationScreen);
        screenHelper.addScreen("bankerDashboard", bankerDashboard);
        screenHelper.addScreen("validationAccount", validationAccount);

        //opens home screen
        Scene mainScene = new Scene(homeScreen);
        screenHelper.setMainScene(mainScene);

        primaryStage.setTitle("Project Bank");
        primaryStage.setScene(mainScene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}