package projectbank.presentation.view;

import javafx.scene.Parent;
import javafx.scene.Scene;

import java.util.HashMap;

public class ScreenHelper {
    private static final ScreenHelper singleton = new ScreenHelper();
    private final HashMap<String, Parent> screenMap = new HashMap<>();
    private Scene mainScene;

    public static ScreenHelper getSingleton() {
        return singleton;
    }

    public void setMainScene(Scene mainScene) {
        this.mainScene = mainScene;
    }

    /**
     * adds a screen to the collection
     *
     * @param name
     * @param node
     */
    protected void addScreen(String name, Parent node) {
        screenMap.put(name, node);
    }

    /**
     * Changes the displayed screen
     *
     * @param screenName
     */
    public void switchScreen(String screenName) {
        Parent newScreen = screenMap.get(screenName);
        if (newScreen == null) {
            throw new RuntimeException("No screen with name " + screenName);
        } else {
            this.mainScene.setRoot(newScreen);
        }
    }
}
