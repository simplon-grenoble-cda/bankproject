package projectbank.business;

import projectbank.data.AccountDAO;
import projectbank.data.CustomerDAO;
import projectbank.domain.Account;
import projectbank.domain.Customer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;


public class CustomerService {
    private static Connection connection;
    private static PreparedStatement preparedStatement;

    /**
     * the method to create a new customer , connect the user after register ,create account
     *
     * @param customer
     */

    public static void createNewCustomer(Customer customer) throws SQLException, ClassNotFoundException {
        CustomerDAO.addCustomer(customer);

        // get the customer and send it to the layer dao "AccountDao"
        Customer customer1 = CustomerDAO.getCustomer(customer.getMail(), customer.getPassword());
        createAccount(customer1);

        // connected the customer after register
        CustomerDAO.checkLogin(customer.getMail(), customer.getPassword());
    }

    /**
     * this is the method to create a new account of the customer after regitser
     *
     * @param customer
     * @throws SQLException
     */
    public static void createAccount(Customer customer) throws SQLException {

        String accountnumber = UUID.randomUUID().toString();
        String iban = "FR38" + accountnumber;
        float balance = 0;
        String createdAT = new SimpleDateFormat("yyyy/MM/dd").format(Calendar.getInstance().getTime());
        String status = "EN_ATTENTE";
        Account account1 = new Account(customer, accountnumber, iban, balance, createdAT, status);
        AccountDAO.addAccount(account1);
    }
}
