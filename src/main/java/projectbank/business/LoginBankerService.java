package projectbank.business;

import projectbank.data.LoginBankerDAO;
import projectbank.domain.Banker;

import java.sql.SQLException;


public class LoginBankerService {
    /**
     * @param mail
     * @param password
     * @return banker after authentication
     * @throws SQLException
     */
    public static Banker loginBanker(String mail, String password) throws SQLException {

        Banker banker = LoginBankerDAO.login(mail, password);

        return banker;
    }
}
