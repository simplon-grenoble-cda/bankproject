
package projectbank.business;

import projectbank.data.*;
import projectbank.domain.Account;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AccountValidationService {

    /**
     *
     * @return list of all account waiting for validation
     */
    public static List<Account> listAllAccountValidation() {

        List<Account> accountList = new ArrayList<>();

        try {
            accountList = AccountDAO.getAccountsToValidate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return accountList;
    }
}

