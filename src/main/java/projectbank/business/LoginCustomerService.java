package projectbank.business;

import projectbank.data.CustomerDAO;
import projectbank.domain.Customer;

import java.sql.SQLException;

public class LoginCustomerService {
    /**
     * @param mail
     * @param password
     * @return customer afer authentication
     * @throws SQLException
     */
    public static Customer loginCustomer(String mail, String password) throws SQLException {

        Customer customer = null;
        try {
            customer = CustomerDAO.checkLogin(mail, password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return customer;
    }
}
